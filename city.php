<?php
if(isset($_POST["state"])){
    $state = $_POST["state"];
    $stateArr = array(
                    "Andhra Pradesh" => array("Amadalavalasa", "Amalapuram", "Amalapuram","Badepalle","Bapatla"),
                    "Bihar" => array("Banka", "Barahiya", "Behea","Bhabua","Bhabua"),
                    "Goa" => array("Curchorem Cacora", "Mapusa", "Panaji","Marmagao")
                );
    if($state !== 'Select'){
        echo "<label>City:</label>";
        echo "<select>";
        foreach($stateArr[$state] as $value){
            echo "<option>". $value . "</option>";
        }
        echo "</select>";
    } 
}
?>
